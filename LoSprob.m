function sigma = LoSprob(Number_MS,Number_BS,d)
sigma=zeros(Number_MS,Number_BS);
for i=1:Number_MS
    for j=1:Number_BS
       sigma(i,j) = min(18/d(i,j),1)*(1-exp(-d(i,j)/63))+exp(-d(i,j)/63);
    end
end
end