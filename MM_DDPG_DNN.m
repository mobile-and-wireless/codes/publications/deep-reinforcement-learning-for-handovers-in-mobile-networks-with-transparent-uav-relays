clc
close all
clear all
Area_x=1000;
Area_y=1000;
min_inter_site_distance = 500;
Number_MS=150;
Number_BS=4;
num_FlyBSs = 4;
NoOfSteps=1000;
Step=1;
Speed_max=3;
f=2;
Pt=23;
Pt_FlyBS=15;
BW = 100*10.^6;
No_TTT_values=40;
Delta_H_values=[0.5 1 1.5 2];
Attenuation_obstacle=0;
Hys = 3;
T310 = 1;
PosBS = [250 250 30; 250 750 30; 750 250 30; 750 750 30];
CIO_t = 0;
CIO_s = 0;
TTT_value=zeros(Number_MS+num_FlyBSs,1); 
FlyBS_CoG = zeros(num_FlyBSs, 2);
Serving_BS=zeros(NoOfSteps,Number_MS+num_FlyBSs);
Serving_BS_SNR=zeros(NoOfSteps,Number_MS+num_FlyBSs);
distances = pdist(PosBS(:, 1:2));
distance_matrix = squareform(distances);
[i, j] = find(distance_matrix < min_inter_site_distance & distance_matrix > 0);
if isempty(i)
    disp("All base stations have minimum inter-site distance of 500m.");
else
    disp("Base stations doesn't have inter-site distance of 500m");
    disp([i, j]);
end
plot(PosBS(1, 1), PosBS(1, 2),'g*');
hold on
plot(PosBS(2, 1), PosBS(2, 2),'g*');
plot(PosBS(3, 1), PosBS(3, 2),'g*');
plot(PosBS(4, 1), PosBS(4, 2),'g*');
xlim([0 1000])
ylim([0 1000])
[UE_positions_whole_area,UE_positions_circular,UE_positions_clusters]=MS_position(Area_x,Area_y,PosBS); % using function for generation of initial coordinates of MSs
plot(UE_positions_whole_area(:,1), UE_positions_whole_area(:,2),'mo');
hold on
plot(UE_positions_circular(1,:), UE_positions_circular(2,:),'ro');
plot(UE_positions_clusters(1,:), UE_positions_clusters(2,:),'bo');
PosMSinit = [UE_positions_whole_area;UE_positions_circular.';UE_positions_clusters.'];
PosMS=movement_PRWMM(length(PosMSinit),NoOfSteps,Step,Speed_max,Area_x,Area_y,PosMSinit); % function for generation of movement
UE_pos = PosMSinit;
%UAVinitial_positions = [450 300 80; 750 850 80; 700 150 80; 100 650 80; 500 700 80; 670 780 80];
UAVinitial_positions = [450 300 80; 750 850 80; 700 150 80; 100 650 80];
PosBS = [PosBS;UAVinitial_positions];
N_W = (BW*4*10.^-12)/10.^9;
N_dBm = 10*log10(N_W/0.001);
c_req = 2e6;
for DeltaT=1:1 
        for SimStep=1:NoOfSteps
            Serving_BS_SNR_help=zeros(Number_MS+num_FlyBSs,2);
            Serving_BS_help=zeros(Number_MS+num_FlyBSs,2);
            d = distance(Number_MS,length(PosBS),PosMS,PosBS,SimStep);
            PL = pathloss(Number_MS,length(PosBS),d,f);
            sigma = LoSprob(Number_MS,length(PosBS),d);
            RSS = RSS_function(Number_MS,length(PosBS),Pt,Pt_FlyBS,PL,sigma,Attenuation_obstacle);
            SNR = SNR_function(Number_MS,length(PosBS),PL,Pt,N_dBm);
            BS_assoc = BS_association(Number_MS,RSS);
            for i=1:Number_MS
              PosMSupdate(i,1) = PosMS(SimStep,2*i-1);
              PosMSupdate(i,2) = PosMS(SimStep,2*i);
            end
            associated_UE_BS5 = BS_assoc(BS_assoc(:,2)==5,:);
            num_associated_UEs5 = numel(associated_UE_BS5(:,1));
            ind_UE5 = find(BS_assoc(:,2)==5);
            associated_UE_BS6 = BS_assoc(BS_assoc(:,2)==6,:);
            num_associated_UEs6 = numel(associated_UE_BS6(:,1));
            ind_UE6 = find(BS_assoc(:,2)==6);              
            associated_UE_BS7 = BS_assoc(BS_assoc(:,2)==7,:);
            num_associated_UEs7 = numel(associated_UE_BS7(:,1));
            ind_UE7 = find(BS_assoc(:,2)==7);
            associated_UE_BS8 = BS_assoc(BS_assoc(:,2)==8,:);
            num_associated_UEs8 = numel(associated_UE_BS8(:,1));
            ind_UE8 = find(BS_assoc(:,2)==8);
            BS_assoc(isnan(BS_assoc(:,1))) = 1;
            %associated_UE_BS9 = BS_assoc(BS_assoc(:,2)==9,:);
            %num_associated_UEs9 = numel(associated_UE_BS9(:,1));
            %ind_UE9 = find(BS_assoc(:,2)==9);
            %BS_assoc(isnan(BS_assoc(:,1))) = 1;                        
            %associated_UE_BS10 = BS_assoc(BS_assoc(:,2)==10,:);
            %num_associated_UEs10 = numel(associated_UE_BS10(:,1));
            %ind_UE10 = find(BS_assoc(:,2)==10);
            %BS_assoc(isnan(BS_assoc(:,1))) = 1;
            CoG_x5 = 0;
            CoG_y5 = 0;
            for j = 1:num_associated_UEs5
                ue_index5 = ind_UE5(j);
                CoG_x5 = CoG_x5 + PosMSupdate(ue_index5, 1);
                CoG_y5 = CoG_y5 + PosMSupdate(ue_index5, 2);
            end              
            CoG_x6 = 0;
            CoG_y6 = 0;
            for j = 1:num_associated_UEs6
                ue_index6 = ind_UE6(j);
                CoG_x6 = CoG_x6 + PosMSupdate(ue_index6, 1);
                CoG_y6 = CoG_y6 + PosMSupdate(ue_index6, 2);
            end
            CoG_x7 = 0;
            CoG_y7 = 0;
            for j = 1:num_associated_UEs7
                ue_index7 = ind_UE7(j);
                CoG_x7 = CoG_x7 + PosMSupdate(ue_index7, 1);
                CoG_y7 = CoG_y7 + PosMSupdate(ue_index7, 2);
            end
            CoG_x8 = 0;
            CoG_y8 = 0;
            for j = 1:num_associated_UEs8
                ue_index8 = ind_UE8(j);
                CoG_x8 = CoG_x8 + PosMSupdate(ue_index8, 1);
                CoG_y8 = CoG_y8 + PosMSupdate(ue_index8, 2);
            end
            %CoG_x9 = 0;
            %CoG_y9 = 0;
            %for j = 1:num_associated_UEs9
            %    ue_index9 = ind_UE9(j);
            %    CoG_x9 = CoG_x9 + PosMSupdate(ue_index9, 1);
            %    CoG_y9 = CoG_y9 + PosMSupdate(ue_index9, 2);
            %end
            %CoG_x10 = 0;
            %CoG_y10 = 0;
            %for j = 1:num_associated_UEs10
            %    ue_index10 = ind_UE10(j);
            %    CoG_x10 = CoG_x10 + PosMSupdate(ue_index10, 1);
            %    CoG_y10 = CoG_y10 + PosMSupdate(ue_index10, 2);
            %end
            FlyBS_CoG(1, 1) = CoG_x5 / num_associated_UEs5;
            FlyBS_CoG(1, 2) = CoG_y5 / num_associated_UEs5;
            FlyBS_CoG(2, 1) = CoG_x6 / num_associated_UEs6;
            FlyBS_CoG(2, 2) = CoG_y6 / num_associated_UEs6; 
            FlyBS_CoG(3, 1) = CoG_x7 / num_associated_UEs7;
            FlyBS_CoG(3, 2) = CoG_y7 / num_associated_UEs7; 
            FlyBS_CoG(4, 1) = CoG_x8 / num_associated_UEs8;
            FlyBS_CoG(4, 2) = CoG_y8 / num_associated_UEs8;
            %FlyBS_CoG(5, 1) = CoG_x9 / num_associated_UEs9;
            %FlyBS_CoG(5, 2) = CoG_y9 / num_associated_UEs9;
            %FlyBS_CoG(6, 1) = CoG_x10 / num_associated_UEs10;
            %FlyBS_CoG(6, 2) = CoG_y10 / num_associated_UEs10;
            PosBS(5:5+num_FlyBSs-1,1:2) = FlyBS_CoG;
            d_UAV = distance_UAVs(num_FlyBSs,length(PosBS(1:Number_BS,:)),FlyBS_CoG,PosBS(1:Number_BS,:));
            PL_UAV = pathloss(num_FlyBSs,length(PosBS(1:Number_BS,:)),d_UAV,f);
            sigma_UAV = LoSprob(num_FlyBSs,length(PosBS(1:Number_BS,:)),d_UAV);
            RSS_UAV = RSS_function(num_FlyBSs,length(PosBS(1:Number_BS,:)),Pt,Pt_FlyBS,PL_UAV,sigma_UAV,Attenuation_obstacle);
            SNR_UAV = SNR_function(num_FlyBSs,length(PosBS(1:Number_BS,:)),PL_UAV,Pt,N_dBm);
            BS_assoc_UAV = BS_association(num_FlyBSs,RSS_UAV);
            plot(PosMSupdate(1:30, 1), PosMSupdate(1:30, 2),'mo');
            plot(PosMSupdate(31:90, 1), PosMSupdate(31:90, 2),'ro');
            plot(PosMSupdate(91:150, 1), PosMSupdate(91:150, 2),'bo');
            plot(FlyBS_CoG(1, 1), FlyBS_CoG(1, 2),"k*");
            plot(FlyBS_CoG(2, 1), FlyBS_CoG(2, 2),"k*");
            plot(FlyBS_CoG(3, 1), FlyBS_CoG(3, 2),"k*");
            plot(FlyBS_CoG(4, 1), FlyBS_CoG(4, 2),"k*");
            %plot(FlyBS_CoG(5, 1), FlyBS_CoG(5, 2),"k*");
            %plot(FlyBS_CoG(6, 1), FlyBS_CoG(6, 2),"k*");            
            targets = BS_assoc(BS_assoc(:,2)>4);
            features = BS_assoc(BS_assoc(:,2)<=4);
            [D2F_gains,~,~] = DNN_D2F_channel_prediction(features,targets);
            ind_D2F = BS_assoc(:,2)>4;
            UE_FlyBS = BS_assoc(ind_D2F==1,2);
            for BS_ass=1:size(UE_FlyBS)
                ind = UE_FlyBS(BS_ass);
                for ue = 1:size(BS_assoc,1)
                if BS_assoc(ue,2)==ind
                BS_assoc(ue,1) = D2F_gains(BS_ass);
                break;
                end
                end
            end
            RSS_Updated = RSS;
            for UE = 1:size(BS_assoc,1) 
            RSS_Updated(UE,BS_assoc(UE,2)) = BS_assoc(UE,1);
            end
            SINR_up = SINR_function(Number_MS,length(PosBS),RSS_Updated,PL,N_W,BS_assoc,Pt,Pt_FlyBS);
            SINR_UAV = SINR_function(num_FlyBSs,length(PosBS(1:Number_BS,:)),RSS_UAV,PL_UAV,N_W,BS_assoc_UAV,Pt,Pt_FlyBS);
            Bn_up = (c_req/1e6)./(log2(1+SINR_up));
            Bn_up = abs(Bn_up);
            Bn = Bn_up*1e6;
            Bn_UAV = (c_req/1e6)./(log2(1+SINR_UAV));
            Bn_UAV = abs(Bn_UAV);
            Bn_U = Bn_UAV*1e6;
            Bn = [Bn;Bn_U];
            BS_assoc = [BS_assoc;BS_assoc_UAV];
            load = zeros(1,length(PosBS));
            for lo = 1:length(PosBS)
                for MS = 1:Number_MS+num_FlyBSs
                  if BS_assoc(MS,2) == lo
                     load(lo) = load(lo) + Bn(MS)/BW;
                  end
                end
            end
            Predicted_CIO = CIO_Decision(length(PosBS),load,Bn,SINR_up,SINR_UAV);
            for j=1:Number_MS+num_FlyBSs
                Serving_BS(SimStep,j)=BS_assoc(j,2);
                Serving_BS_SNR(SimStep,j)=BS_assoc(j,1);
            end            
            if SimStep>1
                for j=1:Number_MS+num_FlyBSs
                    if  Serving_BS(SimStep,j)~=Serving_BS(SimStep-1,j) && Serving_BS_help(j,2)==0
                        Serving_BS_help(j,1)=Serving_BS(SimStep-1,j);
                        Serving_BS_help(j,2)=Serving_BS(SimStep,j);
                        BS_assoc(j,1)=Serving_BS_help(j,1);
                    end
                    if TTT_value(j,1)<(DeltaT) && Serving_BS_help(j,1)~=0
                        Serving_BS_SNR_help(j,1)=SNR(j,Serving_BS_help(j,1));
                        Serving_BS_SNR_help(j,2)=SNR(j,Serving_BS_help(j,2));
                        ind_s = Serving_BS_help(j,2);
                        ind_t = Serving_BS_help(j,1);
                        CIO_s=Predicted_CIO(ind_s);
                        CIO_t=Predicted_CIO(ind_t);
                        CIOs_t = CIO_t-CIO_s;                    
                        if (Serving_BS_SNR_help(j,1)+(10^(Hys/10)))<(Serving_BS_SNR_help(j,2)+CIOs_t)
                            TTT_value(j,1)=TTT_value(j,1)+1;
                            Serving_BS_SNR(SimStep,j)=Serving_BS_SNR_help(j,1);
                        else
                            TTT_value(j,1)=0;
                            Serving_BS_help(j,1)=0;
                            Serving_BS_SNR(SimStep,j)=Serving_BS_SNR_help(j,2);
                        end
                    end
                    if TTT_value(j,1)==DeltaT && Serving_BS_help(j,1)~=0
                        TTT_value(j,1)=0;
                        Serving_BS_help(j,1)=0;
                    end
                end
            end
        end
end