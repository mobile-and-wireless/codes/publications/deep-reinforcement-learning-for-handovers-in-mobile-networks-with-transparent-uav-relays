function action = CIO_Decision(k,cn,Bn,SINR_up,SINR_UAV)
num_episodes = 100;
gamma = 0.99;
actor_lr = 0.001;
critic_lr = 0.01;
CIO_min = -6;
CIO_max = 6;
actornet = create_actor_network(k);
criticnet = create_critic_network(k);
obsInfo = rlNumericSpec([k 1]);
actInfo = rlNumericSpec([k 1],'LowerLimit', 0, 'UpperLimit', 1); 
actor = rlContinuousDeterministicActor(actornet,obsInfo,actInfo);
critic = rlQValueFunction(criticnet,obsInfo,actInfo,...
    ObservationInputNames="netOin", ...
    ActionInputNames="netAin");
agentOptions = rlDDPGAgentOptions(...
    'TargetSmoothFactor', 1e-3, ...
    'SampleTime', 0.01, ...
    'ExperienceBufferLength', 1e6, ...
    'DiscountFactor', 0.99, ...
    'MiniBatchSize', 64, ...
    'NumStepsToLookAhead', 1);
agent = rlDDPGAgent(actor, critic, agentOptions);
trainOpts = rlTrainingOptions(...
    MaxEpisodes=100,...
    MaxStepsPerEpisode=100,...
    StopTrainingCriteria="AverageReward",...
    StopTrainingValue=2000);
replay_buffer = [];
snir=[SINR_UAV;SINR_up];
state = cn;
for episode = 1:num_episodes
        action = select_action(actornet, state, CIO_min, CIO_max);
        [next_state, reward] = perform_action_prop(state, action,Bn,snir,CIO_min,CIO_max);
        experience = struct('state', state, 'action', action, 'reward', reward, 'next_state', next_state);
        replay_buffer = [replay_buffer, experience];
        criticnet = update_critic(actornet, criticnet, replay_buffer, critic_lr, gamma,CIO_min,CIO_max);
        actornet = update_actor(actornet, criticnet, replay_buffer, actor_lr, gamma, CIO_min,CIO_max);
        state = next_state;
end
end