function [predicted_D2F_gains,net,info] = DNN_D2F_channel_prediction(features,targets)
MaxEpochs = 1000;
layers = [sequenceInputLayer(size(features,1))
          fullyConnectedLayer(40)
          sigmoidLayer
          fullyConnectedLayer(30)
          sigmoidLayer
          fullyConnectedLayer(20)
          sigmoidLayer
          fullyConnectedLayer(size(targets,1))
          regressionLayer
];
options = trainingOptions('adam', ...
    'MaxEpochs', MaxEpochs, ...
    'MiniBatchSize', 64, ...
    'InitialLearnRate', 0.01);
options.Verbose = false;
[net, info] = trainNetwork(features, targets, layers, options);
test_data = rand(1, size(features,1));
predicted_D2F_gains = predict(net, test_data.');
end