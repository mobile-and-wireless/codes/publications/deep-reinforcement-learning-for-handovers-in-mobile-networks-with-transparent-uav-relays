function SNR = SNR_function(Number_MS,Number_BS,PL,Pt_MS, N_dBm)
SNR=zeros(Number_MS,Number_BS);
for i=1:Number_MS
    for j=1:Number_BS
       SNR(i,j) = Pt_MS-PL(i,j)-N_dBm;
    end
end
end