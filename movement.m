function PosMS = movement(Number_MS,NoOfSteps,Step,Speed_max,Area_x,Area_y,PosMSinit)
PosMS=zeros(NoOfSteps,2*Number_MS);
Angle= zeros(1,Number_MS);
Speed = zeros(1,Number_MS);
delta_dir_x=ones(1,Number_MS);
delta_dir_y=ones(1,Number_MS);
for i=1:Number_MS
    Angle(1,i)=round(rand*359);
    Speed(1,i)=round(Speed_max*rand);
end
for StepNo=1:NoOfSteps
    if StepNo==1
        for i=1:Number_MS
            PosMS(1,2*i-1) = PosMSinit(1,2*i-1);
            PosMS(1,2*i) = PosMSinit(1,2*i);
        end
    end
    if StepNo>1
        for i=1:Number_MS
            deltaX=delta_dir_x(1,i)*Step*Speed(1,i)*cos(Angle(1,i));
            deltaY=delta_dir_y(1,i)*Step*Speed(1,i)*sin(Angle(1,i));
            PosMS(StepNo,2*i-1)=PosMS(StepNo-1,2*i-1)+deltaX;
            PosMS(StepNo,2*i)=PosMS(StepNo-1,2*i)+deltaY;     
            if PosMS(StepNo,2*i-1)>Area_x
                PosMS(StepNo,2*i-1)=PosMS(StepNo-1,2*i-1)-deltaX;
                if  delta_dir_x(1,i)==1
                    delta_dir_x(1,i)=-1;
                elseif delta_dir_x(1,i)==-1
                     delta_dir_x(1,i)=1;
                end
                deltaX= delta_dir_x(1,i)*Step*Speed(1,i)*cos(Angle(1,i));
                PosMS(StepNo,2*i-1)=PosMS(StepNo-1,2*i-1)+deltaX;       
            elseif PosMS(StepNo,2*i-1)<0
                PosMS(StepNo,2*i-1)=PosMS(StepNo-1,2*i-1)-deltaX;
                 if  delta_dir_x(1,i)==1
                    delta_dir_x(1,i)=-1;
                elseif delta_dir_x(1,i)==-1
                     delta_dir_x(1,i)=1;
                end
                deltaX= delta_dir_x(1,i)*Step*Speed(1,i)*cos(Angle(1,i));
                PosMS(StepNo,2*i-1)=PosMS(StepNo-1,2*i-1)+deltaX;     
            elseif PosMS(StepNo,2*i)>Area_y
                PosMS(StepNo,2*i)=PosMS(StepNo-1,2*i)-deltaY;
                if  delta_dir_y(1,i)==1
                    delta_dir_y(1,i)=-1;
                elseif delta_dir_y(1,i)==-1
                    delta_dir_y(1,i)=1;
                end
                deltaY= delta_dir_y(1,i)*Step*Speed(1,i)*cos(Angle(1,i));
                PosMS(StepNo,2*i)=PosMS(StepNo-1,2*i)+deltaY;
            elseif PosMS(StepNo,2*i)<0
                PosMS(StepNo,2*i)=PosMS(StepNo-1,2*i)-deltaY;
                if  delta_dir_y(1,i)==1
                    delta_dir_y(1,i)=-1;
                elseif delta_dir_y(1,i)==-1
                    delta_dir_y(1,i)=1;
                end
                deltaY= delta_dir_y(1,i)*Step*Speed(1,i)*cos(Angle(1,i));
                PosMS(StepNo,2*i)=PosMS(StepNo-1,2*i)+deltaY;   
            end
        end
    end
end

