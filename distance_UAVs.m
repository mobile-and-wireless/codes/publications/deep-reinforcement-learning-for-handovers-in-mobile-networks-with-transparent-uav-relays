function d = distance_UAVs(Number_MS,Number_BS,PosMS,PosBS)
d=zeros(Number_MS,Number_BS);
for i=1:Number_MS
    for j=1:Number_BS
        d(i,j)=sqrt((abs(PosBS(j,1)-PosMS(i,1)))^2 + (abs(PosBS(j,2)-PosMS(i,2)))^2);
    end
end
end