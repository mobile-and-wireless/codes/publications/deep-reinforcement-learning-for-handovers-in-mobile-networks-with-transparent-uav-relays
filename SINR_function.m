function SINR = SINR_function(Number_MS,Number_BS,RSS, PL, N_W,BS_assoc,Pt,Pt_FlyBS)
SINR=zeros(Number_MS,1);
NI=zeros(Number_MS,1);

for i=1:Number_MS
    for j=1:Number_BS
        if BS_assoc(i,2)~=j
            NI(i,1) = NI(i,1) + 10.^-3*10.^(RSS(i)/10);
        end
    end
    NI(i,1) = NI(i,1) + N_W; 
    NI(i,1)=10*log10(NI(i,1)/0.001);
for j=1:Number_BS
  if BS_assoc(i,2)==j
    if j<=4
        SINR(i,1)=Pt-PL(i,BS_assoc(i,2))-NI(i,1);
    end
    if j>4
        SINR(i,1)=Pt_FlyBS-PL(i,BS_assoc(i,2))-NI(i,1);
    end
  end
end
end