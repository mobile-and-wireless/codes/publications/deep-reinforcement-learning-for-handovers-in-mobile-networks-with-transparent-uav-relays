function gradients = gradient_Actor(network, input_data,reward)
    gradients = dlfeval(@actor_gradients, network, input_data,reward);
end