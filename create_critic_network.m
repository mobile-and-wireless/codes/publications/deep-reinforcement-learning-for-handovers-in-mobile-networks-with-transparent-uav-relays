function criticNet = create_critic_network(k)
obsPath = featureInputLayer(k,Name="netOin");
actPath = featureInputLayer(k,Name="netAin");
commonPath = [
    concatenationLayer(1,2,Name="concat")
    fullyConnectedLayer(50)
    reluLayer
    fullyConnectedLayer(1)
    ];
critic = layerGraph(obsPath);
critic = addLayers(critic, actPath);
critic = addLayers(critic, commonPath);
critic = connectLayers(critic,"netOin","concat/in1");
critic = connectLayers(critic,"netAin","concat/in2");
criticNet = dlnetwork(critic);
end