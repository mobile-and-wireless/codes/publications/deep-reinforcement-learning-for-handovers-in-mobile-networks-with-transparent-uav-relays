function actor = create_actor_network(k)
actorNetwork = [featureInputLayer(k) 
    fullyConnectedLayer(64)
    reluLayer
    fullyConnectedLayer(32)
    reluLayer
    fullyConnectedLayer(1)
    fullyConnectedLayer(k)];
actor = dlnetwork(actorNetwork);
end