function PosMS = movement_PRWMM(Number_MS,NoOfSteps,Step,Speed_max,Area_x,Area_y,PosMSinit)
PosMS=zeros(NoOfSteps,2*Number_MS);
Angle= zeros(1,Number_MS);
Speed = zeros(1,Number_MS);
state=zeros(2,Number_MS);
for i=1:Number_MS
    Angle(1,i)=round(rand*359);
    Speed(1,i)=round(Speed_max*rand);
end
for StepNo=1:NoOfSteps
    if StepNo==1
        for i=1:Number_MS
            PosMS(1,2*i-1) = PosMSinit(i,1);
            PosMS(1,2*i) = PosMSinit(i,2);
        end
    end
    if StepNo>1
        for i=1:Number_MS     
            if state(1,i)==0
                a=rand;
                if a<=0.5
                    state(1,i)=1;
                else
                    state(i,1)=2;
                end
            end
            if state(1,i)==1
                a=rand;
                if a<=0.2
                    state(1,i)=0;
                else
                    state(i,1)=1;
                end
            end
            if state(1,i)==2
                a=rand;
                if a<=0.2
                    state(1,i)=0;
                else
                    state(i,1)=2;
                end
            end
            if state(2,i)==0
                a=rand;
                if a<=0.5
                    state(2,i)=1;
                else
                    state(2,1)=2;
                end
            end
            if state(2,i)==1
                a=rand;
                if a<=0.2
                    state(2,i)=0;
                else
                    state(2,1)=1;
                end
            end
            if state(2,i)==2
                a=rand;
                if a<=0.2
                    state(2,i)=0;
                else
                    state(2,1)=2;
                end
            end
            if state(1,i)==0
                deltaX=0;
            elseif state(1,i)==1
                deltaX=-Step*Speed(1,i)*cos(Angle(1,i));
            else
                deltaX=Step*Speed(1,i)*cos(Angle(1,i));
            end  
            if state(2,i)==0
                deltaY=0;
            elseif state(2,i)==1
                deltaY=-Step*Speed(1,i)*sin(Angle(1,i));
            else
                deltaY=Step*Speed(1,i)*sin(Angle(1,i));
            end
            PosMS(StepNo,2*i-1)=PosMS(StepNo-1,2*i-1)+deltaX;
            PosMS(StepNo,2*i)=PosMS(StepNo-1,2*i)+deltaY; 
            if PosMS(StepNo,2*i-1)>Area_x
                PosMS(StepNo,2*i-1)=0;
                PosMS(StepNo,2*i)=Area_y-PosMS(StepNo-1,2*i);    
            elseif PosMS(StepNo,2*i-1)<0
                PosMS(StepNo,2*i-1)=Area_x;
                PosMS(StepNo,2*i)=Area_y-PosMS(StepNo-1,2*i);
                
            elseif PosMS(StepNo,2*i)>Area_y
                PosMS(StepNo,2*i-1)=Area_x-PosMS(StepNo-1,2*i-1);
                PosMS(StepNo,2*i)=0;
            elseif PosMS(StepNo,2*i)<0
                PosMS(StepNo,2*i-1)=Area_x-PosMS(StepNo-1,2*i-1);
                PosMS(StepNo,2*i)=Area_y;
                
            end
        end
    end
end



