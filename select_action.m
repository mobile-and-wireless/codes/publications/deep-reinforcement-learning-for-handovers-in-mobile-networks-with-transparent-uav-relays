function action = select_action(actor, state, CIO_min, CIO_max)
    raw_action = predict(actor,dlarray(state,'BC'));
    action = ceil(max(min(raw_action, CIO_max), CIO_min));
    for i = 1:length(action)
    CIO_updated(i) = action(i);
    end
    action = CIO_updated;
end
