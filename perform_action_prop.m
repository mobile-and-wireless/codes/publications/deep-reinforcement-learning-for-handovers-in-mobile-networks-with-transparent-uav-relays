function [next_state, reward] = perform_action_prop(state, action,Bn,snr,CIO_min,CIO_max)
    next_state = state + (CIO_min + (CIO_max - CIO_min) * rand(1, size(state,2)));
    mu = 0.1;
    reward = Bn.*log2(1+snr)-mu+mean(action);
end