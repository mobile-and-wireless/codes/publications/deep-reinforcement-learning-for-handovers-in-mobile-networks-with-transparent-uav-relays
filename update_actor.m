function actor = update_actor(actor, ~, batch, actor_lr, ~, ~,~)
    states = cat(1, batch.state);
    action = cat(1, batch.action);
    %reward = cat(1, batch.reward);
    %next_states = cat(1, batch.next_state); 
    actor_gradients = gradient_Actor(actor, dlarray(states,'BC'),action);
    averageGrad = [];
    averageSqGrad = [];    
    actor = adamupdate(actor, actor_gradients,averageGrad,averageSqGrad,1000,actor_lr);
end