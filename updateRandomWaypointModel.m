function new_positions = updateRandomWaypointModel(positions, max_position)
    speed = rand(1, size(positions, 2)) * 3;
    direction = rand(1, size(positions, 2)) * 2 * pi;
    new_positions = positions + [speed .* cos(direction); speed .* sin(direction)];
    new_positions = mod(new_positions, max_position);
end