function gradients = gradient_Critic(network, input_data1,input_data2,target_Q_values)
    gradients = dlfeval(@critic_gradients, network, input_data1, input_data2,target_Q_values);
end