function gradients = actor_gradients(network, input_data,reward)   
    predictions = predict(network, input_data);
    target_Q_values = reshape(reward,[],size(predictions,1));
    loss = crossentropy(dlarray(target_Q_values,'BC'),predictions);
    loss = real(sum(loss));
    gradients = dlgradient(-loss, network.Learnables);
end