function [UE_positions_whole_area,UE_positions_circular,UE_positions_clusters] = MS_position(Area_x,Area_y,PosBS)
radius_around_GBS = 150;
num_UEs_circular = 15;
num_UEs_whole_area = 30;
num_clusters = 6;
radius_cluster = 80;
num_UEs_per_cluster = 10;

UE_positions_circular = [];
for i = 1:size(PosBS, 1)
    theta_circular = linspace(0, 2*pi, num_UEs_circular);
    positions_around_GBS = [PosBS(i, 1) + radius_around_GBS * cos(theta_circular); ...
                             PosBS(i, 2) + radius_around_GBS * sin(theta_circular)];
    UE_positions_circular = [UE_positions_circular positions_around_GBS];
end
UE_positions_whole_area = rand(num_UEs_whole_area,2) * Area_x;
cluster_centers = rand(2, num_clusters) * Area_y;
UE_positions_clusters = [];
for i = 1:num_clusters
    theta_cluster = linspace(0, 2*pi, num_UEs_per_cluster);
    cluster_positions = [cluster_centers(1, i) + radius_cluster * cos(theta_cluster); ...
                         cluster_centers(2, i) + radius_cluster * sin(theta_cluster)];
    UE_positions_clusters = [UE_positions_clusters cluster_positions];
end



