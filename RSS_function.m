function RSS = RSS_function(Number_MS,Number_BS,Pt,Pt_FlyBS,PL,sigma,Attenuation_obstacle)
RSS=zeros(Number_MS,Number_BS);
for i=1:Number_MS
    for j=1:Number_BS
        if j<=4
        if rand<sigma(i,j)
            RSS(i,j) = Pt-PL(i,j);
        else
            RSS(i,j) = Pt-PL(i,j)-Attenuation_obstacle;
        end
        end
        if j>4
        if rand<sigma(i,j)
            RSS(i,j) = Pt_FlyBS-PL(i,j);
        else
            RSS(i,j) = Pt_FlyBS-PL(i,j)-Attenuation_obstacle;
        end
        end
    end
end
end