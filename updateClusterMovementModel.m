function new_positions = updateClusterMovementModel(positions, cluster_centers, radius_cluster, max_position)
    num_clusters = size(cluster_centers, 2);
    num_UEs_per_cluster = size(positions, 2) / num_clusters;
    new_positions = positions;
    for i = 1:num_clusters
        theta_cluster = linspace(0, 2*pi, num_UEs_per_cluster);
        cluster_positions = [cluster_centers(1, i) + radius_cluster * cos(theta_cluster); ...
                             cluster_centers(2, i) + radius_cluster * sin(theta_cluster)];
        new_positions(:, (i-1)*num_UEs_per_cluster+1:i*num_UEs_per_cluster) = cluster_positions;
    end
    new_positions = new_positions + randn(size(new_positions)) * 5;
    new_positions = mod(new_positions, max_position);
end